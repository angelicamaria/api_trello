const   express = require("express"),
        cors = require("cors"),
        bodyParser = require('body-parser'),
        app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

var routes = require('./app/routes/appRoutes');
routes(app); //register the route

app.get('/', function (req, res) {
    res.send('Welcome!');
});

// listen for requests
var port = process.env.PORT || 3001 ;
app.listen(port, () => {
    console.log("Server is listening on port " + port);
});
