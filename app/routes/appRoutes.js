module.exports = (app) => {

    // Task Routes
    const Task = require('../controller/taskController');
    app.route('/tasks')
        .get(Task.list_all_tasks)
        .post(Task.create_a_task);

    app.route('/tasks/:taskId')
        .get(Task.read_a_task)
        .put(Task.update_a_task)
        .delete(Task.delete_a_task);


    // User Routes
    const User = require('../controller/userController');
    app.route('/users')
        .get(User.list_all_users)
        .post(User.create_a_user);

    app.route('/users/:userId')
        .get(User.read_a_user)
        .put(User.update_a_user)
        .delete(User.delete_a_user);
    
};