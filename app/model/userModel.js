const db_connection = require("../config/db");

//User object constructor
class User {

    constructor(user) {
        this.name = user.name;
        this.profile = user.profile;
        this.created_at = new Date();
    }
    static createUser(newUser, result) {
        db_connection.query("INSERT INTO users SET ?", newUser, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            }

            console.log(res.insertId);
            result(null, res.insertId);
        });
    }
    static getUserById(userId, result) {
        db_connection.query("SELECT * FROM users WHERE id = ? ", userId, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            }
            result(null, res);
        });
    }
    static getAllUser(result) {
        db_connection.query("SELECT * FROM users", (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
            }
            else {
                console.log('users : ', res);
                result(null, res);
            }
        });
    }
    static updateById(id, user, result) {
        db_connection.query("UPDATE users SET name = ?, profile = ? WHERE id = ?", [user.name, user.profile, id], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
            }
            else {
                result(null, res);
            }
        });
    }

    static remove(id, result) {
        db_connection.query("DELETE FROM users WHERE id = ?", [id], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
            }
            else {
                result(null, res);
            }
        });
    }

}

module.exports = User;