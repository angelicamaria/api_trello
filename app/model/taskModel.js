const db_connection = require('../config/db');

//Task object constructor
class Task {

    constructor(task) {
        this.task = task.task;
        this.status = task.status;
        this.users = JSON.stringify(task.users);
        this.created_at = new Date();
    }
    static createTask(newTask, result) {
        db_connection.query("INSERT INTO tasks SET ?", newTask, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            }
            console.log(res.insertId);
            result(null, res.insertId);
        });
    }
    static getTaskById(taskId, result) {
        db_connection.query("SELECT * FROM tasks WHERE id = ? ", taskId, (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            }
            result(null, res);
        });
    }
    static getAllTask(result) {
        db_connection.query("SELECT * FROM tasks", (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
            }
            else {
                console.log('tasks : ', res);
                result(null, res);
            }
        });
    }
    static updateById(id, task, result) {
        db_connection.query("UPDATE tasks SET task = ?, status = ?, users = ?  WHERE id = ?", [task.task, task.status, task.users, id], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
            }
            else {
                result(null, res);
            }
        });
    }

    static remove(id, result) {
        db_connection.query("DELETE FROM tasks WHERE id = ?", [id], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
            }
            else {
                result(null, res);
            }
        });
    }

}

module.exports = Task;