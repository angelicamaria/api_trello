const User = require('../model/userModel');

exports.list_all_users = (req, res) => {
    User.getAllUser(function (err, user) {
        console.log('controller')
        if (err) res.send(err);
        console.log('res', user);
        res.send(user);
    });
};

exports.create_a_user = (req, res) => {
    var new_user = new User(req.body);
    if (!new_user.name || !new_user.profile) {
        res.status(400).send({ error: true, message: 'Please provide name/profile' });
    }
    else {
        User.createUser(new_user, function (err, user) {
            if (err) res.send(err);
            res.json(user);
        });
    }
}

exports.read_a_user = (req, res) => {
    User.getUserById(req.params.userId, function (err, user) {
        if (err) res.send(err);
        res.json(user);
    });
}

exports.update_a_user = (req, res) => {
    User.updateById(req.params.userId, new User(req.body), function (err, user) {
        if (err) res.send(err);
        res.json(user);
    });
}

exports.delete_a_user = (req, res) => {
    User.remove(req.params.userId, function (err, user) {
        if (err) res.send(err);
        res.json({ message: 'User successfully deleted' });
    });
}